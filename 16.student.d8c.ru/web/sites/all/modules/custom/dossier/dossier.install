<?php


/**
 * Implements hook_schema().
 */
function dossier_schema() {
  $schema['dossier'] = array(
    'description' => 'Stores information of dossier content.',
    'fields' => array(
      'nid' => array(
        'description' => 'Dossier ID .',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'position' => array(
        'description' => 'Dossier position.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'nid' => array('nid'),
    ),
    'primary key' => array('nid'),
  );

  return $schema;
}


function dossier_node_type_insert($content_type) {
  if ($content_type->type == 'dossier') {
    $body_instance = node_add_body_field($content_type, t('Biography dossier'));


    $body_instance['display']['dossier_list'] = array(
      'label' => 'hidden',
      'type' => 'text_summary_or_trimmed',
    );

    field_update_instance($body_instance);
    // Create all the fields we are adding to our content type.
    foreach (dossier_installed_fields() as $field) {
      drupal_set_message("fields");
      field_create_field($field);
    }

    // Create all the instances for our fields.
    foreach (dossier_installed_instances() as $instance) {
      $instance['entity_type'] = 'node';
      $instance['bundle'] = 'dossier';
      drupal_set_message("instance");
      field_create_instance($instance);
    }
  }
}




/**
 * Define the fields for our content type.
 *
 * This big array is factored into this function for readability.
 *
 * @return array
 *   An associative array specifying the fields we wish to add to our
 *   new node type.
 */
function dossier_installed_fields() {
  return array(
    'dossier_image' => array(
      'field_name' => 'dossier_image',
      'type' => 'image',
      'cardinality' => 1,
    ),
    'dossier_gender' => array(
      'field_name' => 'dossier_gender',
      'type' => 'list_boolean',
      'settings' => array(
        'allowed_values' => array(
          0 => 'Male',
          1 => 'Female',
          2 => 'Other',
        ),
      ),
    ),
  );
}

function dossier_installed_instances() {
  return array(
    'dossier_image' => array(
      'field_name' => 'dossier_image',
      'label' => t('Upload an image:'),
      'required' => FALSE,
      'widget' => array(
        'type' => 'image_image',
        'weight' => 2.10,
      ),
      'display' => array(
        'dossier_list' => array(
          'label' => 'hidden',
          'type' => 'image_link_content__thumbnail',
        ),
      ),
    ),
    'dossier_gender' => array(
      'field_name' => 'dossier_gender',
      'label' => t('The gender for dossier.'),
      'required' => TRUE,
      'widget' => array(
        'settings' => array(),
        'type' => 'options_buttons',
      ),
      'display' => array(
        'dossier_list' => array(
          'label' => 'hidden',
          'type' => 'text',
        ),
      ),
    ),
  );
}