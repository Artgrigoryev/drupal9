<?php


function get_pizza_type() {
    $types = null;
    $result = db_select('pizza_type', 'p')->fields('p')->execute();
    foreach ($result as $row) {
        $pid = $row->pid;

        $name = $row->name;
        $about = $row->about;
        $price = $row->price;
        $available = $row->available;

        $types[$pid]["name"] = $name;
        $types[$pid]["about"] = $about;
        $types[$pid]["price"] = $price;
        $types[$pid]["available"] = $available;
    }

    return $types;
}

function get_pizza_area() {
    $areas = null;
    $result = db_select('pizza_area', 'p')->fields('p')->execute();
    foreach ($result as $row) {
        $aid = $row->aid;

        $name = $row->name;
        $price = $row->price;
        $available = $row->available;

        $areas[$aid]["name"] = $name;
        $areas[$aid]["price"] = $price;
        $areas[$aid]["available"] = $available;
    }
    return $areas;
}

function pizza($form, &$form_state) {
    $types = get_pizza_type();
    $areas = get_pizza_area();

    $form['quantity'] = [
        '#type' => 'fieldset',
        '#tree' => TRUE,
      ];
  
    foreach ($types as $key => $name) {
        $form['quantity'][$key] = [
            '#title' => $name["name"] . '. Цена за штуку: ' . $name["price"],
            '#type' => 'select',
            '#options' => range(0, 10),
        ];
    }

    foreach ($areas as $key => $name) {
        $areas[$key]['name'] .= '. Цена доставки: ' . $areas[$key]['price'];
    }
      
    $form['area'] = [
        '#type' => 'radios',
        '#options' => array_combine(array_keys($areas), array_column($areas, 'name')),
        '#required' => TRUE,
        '#title' => 'Ваш район',
    ];

    $form['phone'] = [
        '#type' => 'tel',
        '#title' => 'Телефон',
        '#required' => TRUE,
    ];
  
    $form['address'] = [
        '#type' => 'textfield',
        '#title' => 'Адрес',
        '#required' => TRUE,
    ];
  
    $form['price'] = [
        '#type' => 'textfield',
        '#title' => t('Price'),
        '#disabled' => TRUE,
        '#id' => 'price',
    ];
  
    $form['submit'] = [
        "#type" => "submit",
        '#value' => t('Отправить'),
    ];
  
    drupal_add_js(drupal_get_path('module', 'pizza') . '/pizza.js');

    $settings = array ( 'types' => $types, 'areas' => $areas, );
    drupal_add_js( array ("pizza" => $settings), 'setting');

    return $form;
}

function pizza_validate($form, &$form_state) {

}
  
function pizza_submit($form, &$form_state) {

}